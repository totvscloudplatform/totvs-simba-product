<!DOCTYPE html>
<html lang="en-US">

<head>

	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
	<!-- SEO -->
	<title>TOTVS SIMBA</title>
	<meta name="description" content="TOTVS SIMBA" />

	<!-- Social & Open Graph -->
	<meta property="og:title" content="TOTVS SIMBA" />

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32">

	<!-- Styles -->
	<link rel='stylesheet' href='assets/css/split.css' type='text/css' media='screen' />
	<meta name="viewport" content="width=device-width,initial-scale=1" />

</head>

<body id="fullsingle" class="page-template-page-fullsingle-split">

<div class="fs-split">

	<!-- Image Side -->
	<div class="split-image">

	</div>

	<!-- Content Side -->
	<div class="split-content">

		<div class="split-content-vertically-center">
		
			<div class="split-intro">
				
				<h1>TOTVS SIMBA</h1>
				<h1>
					<?php
					echo file_get_contents("/tmp/papel.txt");
					?>	
				</h1>

				<span class="tagline">Adult male lion, the king of Pride Rock, and the only son of Mufasa and Sarabi.</span>

			</div>

			<div class="split-bio">
				
				<p>Shortly after his birth, he was anointed future king and presented to the animals of the Pride Lands in a royal presentation ceremony. As the crown prince, he was raised to respect the Circle of Life by his father, Mufasa, and trained in the ways of an upright monarch. During this time, Simba's uncle, Scar, lured him into a wildebeest stampede and used the opportunity to kill Mufasa. Simba blamed himself for the death and exiled himself to the jungle, where he adopted a "Hakuna Matata" lifestyle with Timon and Pumbaa.</p>

			</div>

			<div class="split-lists">
				
				<div class="split-list">

					<h3>Parents</h3>

					<ul>
						<li>Mufasa</li>
						<li>Sarabi</li>
					</ul>

				</div>

				<div class="split-list">

					<h3>Friends</h3>

					<ul>
						<li>Timon</li>
						<li>Pumbaa</li>
					</ul>

				</div>

				<div class="split-list">

					<h3>Enemies</h3>

					<ul>
						<li>Scar</li>
						<li>Hyenas</li>
					</ul>

				</div>
				
			</div>

			<div class="split-credit">

				<p>&copy;2017 <a href="#">TOTVS Simba</a></p>

			</div>		

		</div>

	</div>

</div>

</body>
</html>